from multiprocessing import Process, Queue
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from functools import partial
import pip._internal as pip
try:
    import serial
    import serial.tools.list_ports
except ImportError:
    pip.main(['install', serial])
    pip.main(['install', serial.tools.list_ports])
    import serial
    import serial.tools.list_ports
import time
import sp_logic as logic

class SP_gui:

    subop_num, del_counter = 0, 0
    command_string, Arduino_dir = '', ''
    app_process, subop_init, command_init = False, False, False
    vlabs, alabs, tlabs, add_vels, add_accs, add_times, solabs, sodels, subop_widgets = [], [], [], [], [], [], [], [], []
    command_queue = Queue()

    def __init__(self, master):

        gui_setup = Frame(master, background = '#525252', border = 5)
        gui_setup.grid(row = 0, column = 0, sticky = 'nsew')
        master.grid_rowconfigure(0, weight = 1)
        master.grid_columnconfigure(0, weight = 1)

        upper_frame = Frame(gui_setup, height = 400, width = 800, background = '#525252', border = 5)
        upper_frame.grid(row = 0, sticky = 'nsew')

        lower_frame = Frame(gui_setup, height = 400, width = 800, background = '#525252', border = 5)
        lower_frame.grid(row = 1, sticky = 'nsew')

        self.sdir1 = Label(upper_frame, text = 'No Directory Selected', background = '#F0E68C', borderwidth = 5, relief = 'groove')
        self.sdir1.grid(row = 0, column = 1, ipadx = 30 , padx = 5, pady = 5, stick = 'nsew')

        self.dlab1 = Button(upper_frame, text = 'Click to Select the Directory to the Arduino Folder', command = lambda:self.open_file(upper_frame,self.sdir1), borderwidth = 5, relief = 'groove')
        self.dlab1.grid(row = 0, column = 0, ipadx = 30, padx = 5, pady = 5, stick = 'nsew')

        self.sdir2 = Label(upper_frame, text = 'No Directory Selected', background = '#F0E68C', borderwidth = 5, relief = 'groove')
        self.sdir2.grid(row = 1, column = 1, ipadx = 30 , padx = 5, pady = 5, stick = 'nsew')

        self.dlab2 = Button(upper_frame, text = 'Click to Select the Directory to the Arduino Program', command = lambda:self.open_file(upper_frame,self.sdir2), borderwidth = 5, relief = 'groove')
        self.dlab2.grid(row = 1, column = 0, ipadx = 30, padx = 5, pady = 5, stick = 'nsew') 

        self.nlab = Label(upper_frame, text = 'Enter Suboperation Number: ', borderwidth = 5, relief = 'groove', width = 60)
        self.nlab.grid(row = 2, column = 0, ipadx = 0, padx = 5, pady = 5, stick = 'e')

        self.add_num = Entry(upper_frame, justify = 'center', borderwidth = 5, relief = 'groove', width = 60)
        self.add_num.grid(row = 2, column = 1, ipadx = 30, padx = 5, pady = 5, stick = 'nsew')

        self.add_subop = Button(upper_frame, text = 'Create Suboperation', command = lambda:self.create_subop(lower_frame), borderwidth = 5, relief = 'groove')
        self.add_subop.grid(row = 3, column = 0, columnspan = 2, ipadx = 5, padx = 5, pady = 5, sticky = 'nsew')

        self.up_com = Button(upper_frame, text = 'Update Command String', command = self.update_command, borderwidth = 5, relief = 'groove', state = DISABLED)
        self.up_com.grid(row = 4, column = 0, columnspan = 2, ipadx = 30, padx = 5, pady = 5, sticky = 'nsew')

        self.slab = Label(upper_frame, text = 'Command String: ', borderwidth = 5, relief = 'groove')
        self.slab.grid(row = 5, column = 0, columnspan = 2, ipadx = 30, padx = 5, pady = 5, sticky = 'nsew')

        self.start_app = Button(upper_frame, text = 'Start Application', command = self.start_application, borderwidth = 5, relief = 'groove', background = '#7FFF99', state = DISABLED)
        self.start_app.grid(row = 1, column = 2, rowspan = 2, padx = 5, pady = 5, sticky = 'nsew')

        self.stop_app = Button(upper_frame, text = 'Stop Application', command = lambda:self.stop_application(master), borderwidth = 5, relief = 'groove', background = '#B22222', state = DISABLED)
        self.stop_app.grid(row = 3, column = 2, rowspan = 2, padx = 5, pady = 5, sticky = 'nsew')        
    
    def start_application(self):

        if self.command_string:
            messagebox.showinfo(title = 'Start Operation', message = 'Starting a new operation with the specified parameters')

            self.app_process = True
            self.stop_app.configure(state = NORMAL)

            self.command_queue.put(self.app_process)
            self.command_queue.put(self.command_string)

        else:
            messagebox.showerror(title = 'Command Error', message = 'Cannot start the program without specifying the command string and the directories')

    def stop_application(self, master):

        if self.app_process:          
            messagebox.showinfo(title = 'Stop Operation', message = 'Stopping the operation. User interface will terminate in 10 seconds')

            self.app_process = False

            self.command_queue.put(self.app_process)
            time.sleep(10)
            master.quit()

        else:
            messagebox.showerror(title = 'Command Error', message = 'No operation is available')

    def open_file(self,frame,file_label):
        self.Arduino_dir = filedialog.askopenfilename(title = 'Directory to the File/Folder')

        if self.Arduino_dir:

            file_label.configure(frame, text = self.Arduino_dir)

        else:
            messagebox.showwarning(title = 'File Warning', message = 'No file is selected')
        
    def create_subop(self,frame):
            if not self.add_num.get():

                messagebox.showerror(title = 'Value Error', message = 'Suboperation number is not specified')

                return False

            self.subop_setup()

            try:
                for self.subop_num in range(int(self.add_num.get())):

                    self.render_solab(frame,self.subop_num)
                    self.render_vel(frame,self.subop_num)
                    self.render_acc(frame,self.subop_num)
                    self.render_time(frame,self.subop_num)
                    self.render_sodel(frame,self.subop_num)

                self.subop_widgets = [self.solabs] + [self.vlabs] + [self.add_vels] + [self.alabs] + [self.add_accs] + [self.tlabs] + [self.add_times] + [self.sodels]
                self.subop_init = True

                self.up_com.configure(state = NORMAL)
                self.start_app.configure(state = NORMAL)

            except ValueError:
                messagebox.showerror(title = 'Value Error', message = 'Suboperation numbers can only be positive integers')

    def render_solab(self,frame,subop_num):

        solab = Label(frame, text = 'Suboperation {}'.format(subop_num + 1), background = '#FFE4B5', borderwidth = 5, relief = 'groove')
        solab.grid(row = subop_num, column = 0, padx = 15, pady = 5, stick = 'nsew')
        self.solabs.append(solab)

    def render_vel(self,frame,subop_num):

        vlab = Label(frame, text = 'Target Velocity (rpm): ', background = '#F0E68C', borderwidth = 5, relief = 'groove')
        vlab.grid(row = subop_num, column = 1, padx = 5, pady = 5, stick = 'nsew')
        self.vlabs.append(vlab)

        add_vel = Entry(frame, justify = 'center', borderwidth = 5, relief = 'groove', width = 15)
        add_vel.grid(row = subop_num, column = 2, padx = 5, pady = 5, stick = 'nsew')
        self.add_vels.append(add_vel)

    def render_acc(self,frame,subop_num):

        alab = Label(frame, text = 'Acceleration Time (s): ', background = '#F0E68C', borderwidth = 5, relief = 'groove')
        alab.grid(row = subop_num, column = 3, padx = 5, pady = 5, stick = 'nsew')
        self.alabs.append(alab)

        add_acc = Entry(frame, justify = 'center', borderwidth = 5, relief = 'groove', width = 15)
        add_acc.grid(row = subop_num, column = 4, padx = 5, pady = 5, stick = 'nsew')
        self.add_accs.append(add_acc)

    def render_time(self,frame,subop_num):

        tlab = Label(frame, text = 'Idle Time (s): ', background = '#F0E68C', borderwidth = 5, relief = 'groove')
        tlab.grid(row = subop_num, column = 5, padx = 5, pady = 5, stick = 'nsew')
        self.tlabs.append(tlab)

        add_time = Entry(frame, justify = 'center', borderwidth = 5, relief = 'groove', width = 15)
        add_time.grid(row = subop_num, column = 6, padx = 5, pady = 5, stick = 'nsew')
        self.add_times.append(add_time)

    def render_sodel(self,frame,subop_num):

        action_arg = partial(self.delete_subop, subop_num)
        sodel = Button(frame, text = 'X', command = action_arg, background = '#F0E68C', borderwidth = 5, relief = 'groove')
        sodel.grid(row = subop_num, column = 7, padx = 5, pady = 5, stick = 'nsew')
        self.sodels.append(sodel)

    def subop_setup(self):
        if self.subop_init:
            if messagebox.askyesnocancel(title = 'Create Suboperations', message = 'Creating new suboperations will overwrite existing ones, do you wish to proceed?'): 
                for sub_widget_num in range(len(self.subop_widgets)):
                    for dsub_widget in self.subop_widgets[sub_widget_num]:
                        dsub_widget.destroy()

                self.vlabs, self.alabs, self.tlabs, self.add_vels, self.add_accs, self.add_times, self.solabs, self.sodels, self.subop_widgets = [], [], [], [], [], [], [], [], []            
                self.del_counter = 0

    def delete_subop(self,subop):
        subop = subop - self.del_counter

        for _ in range(self.subop_num + 1):
            if _ <= subop:
                continue
            else:
                self.solabs[_].configure(text = 'Suboperation {}'.format(_))
            
        for sub_widget_num in range(len(self.subop_widgets)):
            print(sub_widget_num)
            print(subop)

            self.subop_widgets[sub_widget_num][subop].destroy()
            self.subop_widgets[sub_widget_num].pop(subop)

        self.subop_num -= 1
        self.del_counter += 1

    def update_command(self):
        self.command_setup()

        for _ in range(self.subop_num + 1):
            if self.add_vels[_].get() and self.add_accs[_].get() and self.add_times[_].get():
                if self.add_vels[_].get().isdecimal() and self.add_accs[_].get().isdecimal() and self.add_times[_].get().isdecimal():
                    self.command_string = self.command_string + self.add_vels[_].get() + '.' + self.add_accs[_].get() + '.' + self.add_times[_].get() + '-'
                else:
                    messagebox.showerror(title = 'Command Error', message = 'All variables must either be positive integers or 0')

                    return
            else:
                messagebox.showerror(title = 'Command Error', message = 'All variables of all suboperations must be given to form a command string')

                return

        self.slab.configure(text = 'Command String: {}'.format(self.command_string))
        self.command_init = True

    def command_setup(self):
        self.command_string = ''

        if self.command_init:
            if messagebox.askyesnocancel(title = 'Create Command String', message = 'Creating a new command string will overwrite the existing one, do you wish to proceed?'): 
                self.slab.configure(text = 'Command String: {}'.format(self.command_string))

    def get_dir(self):

        return self.Arduino_dir

    def get_command(self):

        return self.command_string

def main():

    root = Tk()
    root.title('SP Studio')
    root.geometry('1200x800')
    gui = SP_gui(root)

    logic_process = Process(target = logic.main, args = (gui.command_queue,))
    logic_process.start()

    root.mainloop()
    
if __name__ == '__main__':
    main()

    



