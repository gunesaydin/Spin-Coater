import time
import math
import subprocess
import sys
from multiprocessing import Process, Queue

subprocess.check_call([sys.executable, '-m', 'pip', 'install', '--upgrade', 'pip', 'wheel'])

try:
    import pyfirmata
    import serial.tools.list_ports
    print('---------------modules are already installed, importing---------------\n')

except ImportError:
    print('--installing modules--')
    time.sleep(1)
    subprocess.check_call([sys.executable, "-m", "pip", "install", 'pyfirmata'])
    print('--------------------------importing modules--------------------------\n')
    time.sleep(1)
    import pyfirmata
    import serial.tools.list_ports
    print('---------------------------------done--------------------------------\n')
    
def get_ports():

    ports = serial.tools.list_ports.comports()
    return ports

def find_Arduino_start_serial(ports_found):

    if ports_found:

        for port in ports_found:
            str_port = str(port)
            print('printing port')
            print(port)
            print(type(port))
            print(str_port)
            print(type(str_port))
            if 'Arduino' in str_port:
                print('yesss')
                #port_selection = port.split('')[0]
                port_selection = str_port.split()[0]
                print(port_selection)
                return port_selection
    
def read_command_string(command_string):
    global subop_commands, tasks
    revised_command_string = []

    subop_commands = command_string.split('-')

    for subop_counter, command in enumerate(subop_commands):

        revised_command_string.insert(subop_counter, [])

        tasks = command.split('.')
         
        for task_counter, task in enumerate(tasks):

            revised_command_string[subop_counter].insert(task_counter, task)
        
    return revised_command_string
    
def rpm_to_esc_angle(rpm):
    global max_angle, max_speed, adjusted_angle
    max_angle, max_speed = 180, 10000

    pre_angle = rpm*max_angle/max_speed
    adjusted_angle = math.ceil(pre_angle)

    return adjusted_angle

def setup_and_start_program(board,revised_command_string,gui_queue):
    global ESC_PIN, ESC_PWM_LOW, ESC_PWM_HIGH, servo, current_vel, max_acc
    ESC_PIN, ESC_PWM_LOW, ESC_PWM_HIGH, current_vel, max_acc = 9, 1000, 2000, 0, 3

    servo = board.get_pin('d:{}:s'.format(ESC_PIN))
    
    for subop in revised_command_string:

        subop_vel = rpm_to_esc_angle(subop[0])
        subop_acc = subop[1]
        subop_time = subop[2]

        while current_vel != subop_vel:

            current_vel = current_vel + (subop_vel - current_vel)/subop_acc
            servo.write(current_vel)

            if gui_queue.get() == False:
                print('stopping execution')

                while current_vel == 0:
                    current_vel = current_vel/max_acc
                    servo.write(current_vel)

        time.sleep(subop_time*1000)
        
def main(gui_queue):

    logic_queue = Queue()

    data_stream = gui_queue.get()
    print(data_stream)

    command_string = gui_queue.get()
    print(command_string)

    ports_found = get_ports()
    port_selection = find_Arduino_start_serial(ports_found)

    board = pyfirmata.Arduino(port_selection)
    revised_command_string = read_command_string(command_string)

    logic_queue.put('START')

    setup_and_start_program(board, revised_command_string,gui_queue)

    logic_queue.put('END')
