#include <Servo.h>

const byte buff_size = 40, max_subop = 10;
char input_buffer[buff_size], char message_from_pc[buff_size] = {0};

byte bytes_received = 0, call_num = 0;
boolean read_in_progress = false, new_data_from_pc = false;

const char start_marker = '<', end_marker = '>', subop_sep = "-", task_sep = "."

const int min_speed = 0; max_speed = 10000;
int subop_count = 0, task_count = 0;
char subop_buffer[max_subop][20];
int vel_command[max_subop], acc_command[max_subop], time_command[max_subop];

Servo sp;

void setSpeed(int speed){
  int speed_var = map(speed, min_speed, max_speed, 0, 180);
  sp.write(speed_var);
}

void arm(){
  setSpeed(10000);
  delay(5000);
  
  setSpeed(0);
  delay(5000);
}

void get_serial_data(){
  
  if (Serial.available() > 0){
    
    char x = Serial.read();

    if (x == end_marker){
      
      read_in_progress = false;
      new_data_from_pc = true;
      input_buffer[bytes_received] = 0;
      parse_data();
      
    }

    if (read_in_progress){
      
      input_buffer[bytes_received] = x;
      bytes_received ++;
      if (bytes_received == buff_size){
        bytes_received = buff_size - 1;
        
      }
    }

    if (x == start_marker){
      
      bytes_received = 0;
      read_in_progress = true;
      
    }
  }  
}

void parse_data_to_suboperations(){
  
  char * strtok_index;
  
  strtok_index = strtok(input_buffer, subop_sep);
  
  while (strtok_index != NULL){
    
    strcpy(subop_buffer[subop_count],strtok_index); 
    strtok_index = strtok(NULL, subop_sep);
    subop_count++;
    
  }

void seperate_suboperations_to_tasks(){
  
  int vel, acc, tim, subop_counter = 0;
  char * resolved_task;
  
  while (counter != subop_count){
    
    resolved_task = strtok(subop_buffer[counter], task_sep)      
    vel = atoi(resolved_task)
    vel_command[counter] = vel
    
    resolved_task = strtok(NULL, task_sep)
    acc = atoi(resolved_task)
    acc_command[counter] = acc
  
    resolved_task = strtok(NULL, task_sep)
    tim = atoi(resolved_task)
    time_command[counter] = tim

    counter ++
    }
  }
  
}

void create_suboperations_and_tasks(subop_vel_command, subop_acc_command, subop_time_command){

  int vel = 0;

  while (vel < subop_vel_command){
    sp.write(vel);
    vel++;
    delay(subop_acc_command/(subop_vel_command - vel)*1000);
  }
  delay(subop_time_command);
  
}

void start_program(vel_cm, acc_cm, time_cm){

  int subop_counter = 0;

  while (subop_counter <= subop_num){
    create_suboperations_and_tasks(vel_cm[subop_counter], acc_cm[subop_counter], time_cm[time_counter]);
    subop_counter++;
  }

void reply_to_serial(){
  if (new_data_from_pc) {
    new_data_from_pc = false;
    Serial.print("Operation data parsed, starting operation...");
  }
}
void setup(){
  Serial.begin(9600);
  Serial.setTimeout(1000);
  sp.attach(9, 1000, 2000);
  arm();

  Serial.println("<Arduino is ready>");
}

void loop(){
  get_serial_data();
  parse_data_to_suboperations();
  seperate_suboperations_to_tasks;
  start_program(vel_command, acc_command, time_command);
  reply_to_serial();
  }
