import subprocess
import pip._internal as pip
try:
    import serial
    import serial.tools.list_ports
except ImportError:
    pip.main(['install', serial])
    pip.main(['install', serial.tools.list_ports])
    import serial
    import serial.tools.list_ports
from multiprocessing import Process, Queue

def get_ports():
    ports = serial.tools.list_ports.comports()
    return ports

def find_Arduino_start_serial(ports_found, baud, time, gui):
    global ser, port_selection
    if ports_found:
        for COMnum in range(0, len(ports_found)):
            port = str(ports_found[COMnum])
            if 'Arduino' in port:
                port_selection = port.split(' ')[0]
                ser = serial.Serial(port_selection, baudrate = baud, timeout = time)                  
    else:
        gui.messagebox.showerror(title = 'Connection Error', message = 'Arduino is not connected or cannot be found, check the available COM ports')
        return

def build_and_upload_Arduino_sketch(Arduino_directory):
    global Arduino_prog, Arduino_command
    Arduino_prog = 'C:\\Users\\Gunes\\Desktop\\Arduino\\arduino_debug.exe'
    project_file = Arduino_directory

    Arduino_command = Arduino_prog + ' --' + 'upload' + ' --board ' + 'Arduino Uno' + ' --port ' + port_selection + ' ' + project_file

    subprocess.call(Arduino_command, shell = True)

def send_to_Arduino(send_str):
    ser.write(send_str)

def receive_from_Arduino():
    ck, x, byte_count = '', 'z', -1

    while ord(x) != start_marker:
        x = ser.read()
    while ord(x) != end_marker:
        if ord(x) != start_marker:
            ck.append(x)
            byte_count += 1
        x = ser.read()
    return(ck)

def wait_for_Arduino():
    Arduino_message = ''
    while Arduino_message.find('Arduino is ready') == 0:
        while ser.inWaiting() == 0:
            pass
    Arduino_message = receive_from_Arduino()

def main(gui_queue):
    global baudrate, timeout, start_marker, end_marker
    baudrate, timeout, start_marker, end_marker = 9600, 1, 60, 62

    print('wow1')
    #while not gui_queue.get():
    #    time.sleep(5)
    #    print('wow')

    print('wow2')
    data_stream = gui_queue.get()
    print(data_stream)
    print('incredible')
    data = gui_queue.get()
    print(data)
    print('wow')

    #while gui_queue.get():
    #    time.sleep(5)
    #    print('wow3')

    if gui_queue.get() == False:
        print('stopping execution')

    #Arduino_dir = sp_gui.Arduino_dir

    #ports_available = get_ports
    #find_Arduino_start_serial(ports_available, baudrate, timeout, sp_gui)
    #build_and_upload_Arduino_sketch(Arduino_dir)
    #wait_for_Arduino()
    #send_to_Arduino(data)
    #receive_from_Arduino()

    #while gui.SP.app_process:
    #    print('wowx2')
    #    time.sleep(5)
    #    continue
    #ser.close



